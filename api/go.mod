module gitlab.com/Errvald/kaffes

go 1.13.5

require (
	github.com/go-chi/chi v4.0.4+incompatible
	github.com/go-chi/render v1.0.1
	go.mongodb.org/mongo-driver v1.3.1
)
