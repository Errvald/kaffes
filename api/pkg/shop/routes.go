package shop

import (
	"net/http"

	"go.mongodb.org/mongo-driver/mongo"
	"github.com/go-chi/chi"
)

func Routes(c *mongo.Database) *chi.Mux {
	ShopCollection(c)

	router := chi.NewRouter()
	router.Get("/{shopID}", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("show"))
	})
	router.Delete("/{shopID}", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("destroy"))
	})
	router.Post("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("create"))
	})
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("index"))
	})
	return router
}