package shop

type Shop struct {
	// ID        entity.ID    `json:"id" bson:"_id,omitempty"`
	ID        int       `json:"id"`
	Name      string    `json:"name"`
}