package shop

import (
	"log"
	"context"
	 
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// type Service interface {
	// Show(shop *Shop) (entity.ID, error)
	// List(user *User) error
	// Store(user *User, password string) error
	// Destroy(user *User) error
	// Update(user *User, password string) error
// }

type Todo struct {
	ID        string    `json:"id"`
	Title     string    `json:"title"`
	// Body      string    `json:"body"`
	// Completed string    `json:"completed"`
	// CreatedAt time.Time `json:"created_at"`
	// UpdatedAt time.Time `json:"updated_at"`
}

var collection *mongo.Collection

func ShopCollection(c *mongo.Database) {
	collection = c.Collection("shops")
}

func Show() {

}

func GetAllShops() {
	todos := []Todo{}
	cursor, err := collection.Find(context.TODO(), bson.M{})

	if err != nil {
		log.Printf("Error while getting all todos, Reason: %v\n", err)
		return
	}

	// Iterate through the returned cursor.
	for cursor.Next(context.TODO()) {
		var todo Todo
		cursor.Decode(&todo)
		todos = append(todos, todo)
		log.Printf("todo: %v\n", todo)
	}
	return
}