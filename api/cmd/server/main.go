package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"

	shop "gitlab.com/Errvald/kaffes/pkg/shop"
	database "gitlab.com/Errvald/kaffes/pkg/database"
	// "compress/flate"
)


func Routes() *chi.Mux {
	db := database.NewMongoClient()
	
	r := chi.NewRouter()

	// TODO: Compress
	// compressor := middleware.NewCompressor(flate.DefaultCompression)
	// r.Use(compressor.Handler())
	r.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.RedirectSlashes,
		middleware.Recoverer,
	)
	r.Mount("/shops", shop.Routes(db))
	return r
}


func main() {
	router := Routes()
	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route) // Walk and print out all routes
		return nil
	}
	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("Logging err: %s\n", err.Error())
	}
	log.Fatal(http.ListenAndServe(":5000", router))
}
